## 0.8.5
* fix for ordinary users trying to update the game settings on load.

## 0.8.4
* about-time should automatically synchronise to the pf2e world time and stay in step once loaded. You don't need to set any offset fields, the pf2e world creation date/time is used by about-time.

## 0.8.3
* correct offset for m/d in zero time offset.

## 0.8.2
* Updated various language translations as well, thanks to @klo, benwater12 and Jose Lazano.

## 0.8.1
* Added World Time 0 calendar date/time offset. This allows you to specify an arbitrary game time that corresponds to game.time.worldtime = 0, format is y/m/d h:m:s. Should help with PF2E date/time problems.

## 0.8.0
Some html cleanup.
abut-time seems to be 0.8.x compatible.

## 0.1.68
* Added named years support. Internally all calculations are done with numeric years. On output a given year number is converted to a year name assuming year names cycle starting at 0.
* Added support for year names in calendar editor.
* See Readme.md for example usage.
* Added sample calendar's for Dark Sun and Nehwon.
* Updated es.json - thanks Jose Lozano

